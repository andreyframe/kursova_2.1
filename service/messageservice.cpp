#include "messageservice.h"

MessageService::MessageService(QWidget* parent) :
    QWidget(parent)
{

}

void MessageService::ShowError(QString message)
{
    QMessageBox::critical(this, "Error", message);
}

void MessageService::Information(QString message)
{
    QMessageBox::information(this, "Info", message);
}
