#ifndef DATA_H
#define DATA_H

#include <list>
#include <iterator>
#include "models/sellermodel.h"
#include "models/adminmodel.h"
#include "models/foodmodel.h"
#include <QTextStream>
#include <typeinfo>
#include <cxxabi.h>
#include <QFile>

#include <QDebug>

template<class T>
class Data
{
private:
    static std::list<T> data;
    static int count;
public:
    Data();
    static void initData();
    static void saveData();
    static int getCount();
    static std::list<T> getAll();
    static T* add(T* _item);
    static T* getByIndex(int);
    static T* getById(int);
    static T* updateById(T data, int id);
};

#endif // DATA_H
