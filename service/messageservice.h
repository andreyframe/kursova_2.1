#ifndef MESSAGESERVICE_H
#define MESSAGESERVICE_H

#include <QString>
#include <QMessageBox>
#include <QWidget>

class IMessageService
{
public:
    virtual void ShowError(QString message) = 0;
    virtual void Information(QString message) = 0;
};

class MessageService : public QWidget, public IMessageService
{
public:
    MessageService(QWidget* parent = 0);
    void ShowError(QString message = "Oops");
    void Information(QString message = "Oops");
};

#endif // MESSAGESERVICE_H
