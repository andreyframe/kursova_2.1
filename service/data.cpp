#include "data.h"

template <class T>
int Data<T>::count = 0;

template <class T>
std::list<T> Data<T>::data;

template<class T>
Data<T>::Data()
{

}

template<class T>
void Data<T>::initData()
{
    QString name = abi::__cxa_demangle(typeid(T).name(), 0, 0, 0);

    QFile file;
    file.setFileName(name + ".txt");
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);
    T obj;

    if(name == "AdminModel"){
        while(!stream.atEnd())
        {
            stream >> obj;
            if(obj.getId() == 0) continue;
            data.push_back(obj);
        }
        qDebug() << data.size() + 0.1;
    }else if(name == "SellerModel"){
        while(!stream.atEnd())
        {
            stream >> obj;
            if(obj.getId() == 0) continue;
            data.push_back(obj);
        }
        qDebug() << data.size() + 0.2;
    }else if(name == "FoodModel"){
        while(!stream.atEnd())
        {
            stream >> obj;
            if(obj.getId() == 0) continue;
            data.push_back(obj);
        }
        qDebug() << data.size() + 0.3;
    }
    count = data.size();
    file.close();
}

template<class T>
void Data<T>::saveData()
{
    QString name = abi::__cxa_demangle(typeid(T).name(), 0, 0, 0);

    QFile file;
    file.setFileName(name + ".txt");
    file.open(QIODevice::WriteOnly);
    QTextStream stream(&file);


    if(name == "AdminModel"){
        typename std::list<T>::iterator iter;
        for(iter = data.begin(); iter != data.end(); iter++)
        {
            stream << (*iter);
        }
    }else if(name == "SellerModel"){
        typename std::list<T>::iterator iter;
        for(iter = data.begin(); iter != data.end(); iter++)
        {
            stream << (*iter);
        }
    }else if(name == "FoodModel"){
        typename std::list<T>::iterator iter;
        for(iter = data.begin(); iter != data.end(); iter++)
        {
            stream << (*iter);
        }
    }
    file.close();
}

template <class T>
int Data<T>::getCount(){
    return count;
}

template<class T>
std::list<T> Data<T>::getAll()
{
    return Data<T>::data;;
}

template <class T>
T* Data<T>::add(T *_item){
    _item->setId(++count);
    data.push_back(*_item);
    return _item;
}

template <class T>
T* Data<T>::getByIndex(int i){
    if (i < data.size())
    {
        typename std::list<T>::iterator iter = data.begin();
        std::advance(iter, i);
        return &(*iter);
    } else return NULL;
}

template<class T>
T* Data<T>::getById(int id)
{
    typename std::list<T>::iterator iter = data.begin();
    for(; iter != data.end(); ++iter){
        if(iter->getId() == id) return &(*iter);
    }
    return NULL;
}

template<class T>
T *Data<T>::updateById(T _data, int id)
{
    typename std::list<T>::iterator iter = data.begin();
    for(; iter != data.end(); ++iter){
        if(iter->getId() == id) {
            data.insert(iter, _data);
            iter = data.erase(iter);
            return &(*iter);
        }
    }
    return NULL;
}

template class Data<SellerModel>;
template class Data<AdminModel>;
template class Data<FoodModel>;
