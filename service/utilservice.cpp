#include "utilservice.h"

UtilService::UtilService() :
    email("^[A-Za-z0-9._+-]{4,60}@[A-Za-z0-9.-]{1,20}\\.[A-Za-z]{2,4}$"),
    name("^[A-Za-z]{2,10}$"),
    password("^[a-zA-Z0-9 ]{6,40}$")
{

}
