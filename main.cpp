#include <QApplication>
#include <QString>
#include "sign/signin.h"
#include "sign/signup.h"
#include "sign/sign_presenter.h"
#include "models/sellermodel.h"
#include "service/messageservice.h"
#include "sign/signservice.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Data<AdminModel>::initData();
    Data<SellerModel>::initData();
    Data<FoodModel>::initData();

    SignIn* view_signin = new SignIn();
    SignUp* view_signup = new SignUp();
    SignService* service = new SignService();
    /*service->registerUser(new AdminModel("Admin",
                                          "qweqwe@qwe.qwe",
                                          "qweqwe",
                                             "Admin"));
    service->registerUser(new SellerModel("Seller",
                                          "asdasd@asd.asd",
                                          "asdasd",
                                             "Seller"));
    Data<FoodModel>::add(new FoodModel("Bananna", 12.2));
    Data<FoodModel>::add(new FoodModel("Apple", 20));
    Data<FoodModel>::add(new FoodModel("Ap_Ba", 15));*/
    MessageService* message = new MessageService();
    Sign_Presenter* pres = new Sign_Presenter(view_signin, view_signup, service, message);
    view_signin->show();


    a.exec();

    Data<AdminModel>::saveData();
    Data<SellerModel>::saveData();
    Data<FoodModel>::saveData();


    return 0;
}
