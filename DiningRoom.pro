#-------------------------------------------------
#
# Project created by QtCreator 2017-11-07T22:41:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DiningRoom
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    administrator/admin_presenter.cpp \
    administrator/admin_service.cpp \
    administrator/administrator.cpp \
    models/sellermodel.cpp \
    models/user.cpp \
    seller/seller.cpp \
    seller/seller_presenter.cpp \
    seller/sellerservice.cpp \
    service/data.cpp \
    service/messageservice.cpp \
    service/utilservice.cpp \
    sign/sign_presenter.cpp \
    sign/signin.cpp \
    sign/signservice.cpp \
    sign/signup.cpp \
    main.cpp \
    models/adminmodel.cpp \
    models/foodmodel.cpp \
    models/baseentity.cpp \
    administrator/addfood.cpp

HEADERS += \
    administrator/admin_presenter.h \
    administrator/admin_service.h \
    administrator/administrator.h \
    models/sellermodel.h \
    models/user.h \
    seller/seller.h \
    seller/seller_presenter.h \
    seller/sellerservice.h \
    service/data.h \
    service/messageservice.h \
    service/utilservice.h \
    sign/sign_presenter.h \
    sign/signin.h \
    sign/signservice.h \
    sign/signup.h \
    models/adminmodel.h \
    models/foodmodel.h \
    models/baseentity.h \
    administrator/addfood.h

FORMS += \
    administrator.ui \
    seller.ui \
    signin.ui \
    signup.ui \
    administrator/addfood.ui
