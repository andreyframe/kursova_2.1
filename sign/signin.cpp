#include "signin.h"
#include "ui_signin.h"

SignIn::SignIn(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SignIn)
{
    ui->setupUi(this);
    //this->setLayout(ui->layout_main);

    ui->inputEmail->setText("qweqwe@qwe.qwe");
    ui->inputPassword->setText("qweqwe");

    setWindowTitle("Sign In");
    connect(ui->btn_register, SIGNAL(clicked()), this, SLOT(closeAndSendSignal()));
    connect(ui->btn_exit, SIGNAL(clicked(bool)), this, SLOT(close()));
    connect(ui->btn_login, SIGNAL(clicked(bool)), this, SIGNAL(clickedSignIn()));
}

SignIn::~SignIn()
{
    delete ui;
}

void SignIn::closeAndSendSignal()
{
    this->close();
    emit viewSignUpShow();
}

QString SignIn::getEmail()
{
    return ui->inputEmail->text();
}

QString SignIn::getPassword()
{
    return ui->inputPassword->text();
}
