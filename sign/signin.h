#ifndef SIGNIN_H
#define SIGNIN_H

#include <QWidget>
#include <QString>
#include <QDebug>

namespace Ui {
class SignIn;
}

class ISignIn {
public:
    virtual QString getEmail() = 0;
    virtual QString getPassword() = 0;
};

class SignIn : public QWidget, public ISignIn
{
    Q_OBJECT

public:
    explicit SignIn(QWidget *parent = 0);
    ~SignIn();
    QString getEmail();
    QString getPassword();
signals:
    void viewSignUpShow();
    void clickedSignIn();

public slots:
    void closeAndSendSignal();

private:
    Ui::SignIn *ui;
};

#endif // SIGNIN_H
