#ifndef SIGNUP_H
#define SIGNUP_H

#include <QWidget>
#include <QString>
#include <QPushButton>
#include <QDebug>

namespace Ui {
class SignUp;
}

class ISignUp {
public:
    virtual QString getFullName() = 0;
    virtual QString getEmail() = 0;
    virtual QString getPassword() = 0;
    virtual QString getConfirmPassword() = 0;
    virtual QString  getRole() = 0;
    virtual void setFullName(QString) = 0;
    virtual void setEmail(QString) = 0;
    virtual void setPassword(QString) = 0;
    virtual void setConfirmPassword(QString) = 0;
    virtual QPushButton* getButton() = 0;
};

class SignUp : public QWidget, public ISignUp
{
    Q_OBJECT

public:
    explicit SignUp(QWidget *parent = 0);
    ~SignUp();
    QString getFullName();
    QString getEmail();
    QString getPassword();
    QString getConfirmPassword();
    QString getRole();
    void setFullName(QString);
    void setEmail(QString);
    void setPassword(QString);
    void setConfirmPassword(QString);
    virtual QPushButton* getButton();

signals:
    void viewSignInShow();
    void clickSignUp();
    void changeForm();

public slots:
    void closeAndSendSignal();

private:
    Ui::SignUp *ui;
};

#endif // SIGNUP_H
