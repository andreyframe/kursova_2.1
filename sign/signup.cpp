#include "signup.h"
#include "ui_signup.h"

SignUp::SignUp(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SignUp)
{
    ui->setupUi(this);

    setWindowTitle("Sign Up");

    connect(ui->btn_back, SIGNAL(clicked()), this, SLOT(closeAndSendSignal()));

    connect(ui->btnSignUp, SIGNAL(clicked()), this, SIGNAL(clickSignUp()));
    connect(ui->inputFullName, SIGNAL(textChanged(QString)), this, SIGNAL(changeForm()));
    connect(ui->inputEmail, SIGNAL(textChanged(QString)), this, SIGNAL(changeForm()));
    connect(ui->inputPassword, SIGNAL(textChanged(QString)), this, SIGNAL(changeForm()));
    connect(ui->inputConfirmPassword, SIGNAL(textChanged(QString)), this, SIGNAL(changeForm()));
}

SignUp::~SignUp()
{
    delete ui;
}

QString SignUp::getFullName()
{
    return ui->inputFullName->text();
}

QString SignUp::getEmail()
{
    return ui->inputEmail->text();
}

QString SignUp::getPassword()
{
    return ui->inputPassword->text();
}

QString SignUp::getConfirmPassword()
{
    return ui->inputConfirmPassword->text();
}

QString SignUp::getRole()
{
    return ui->comboBoxRole->currentText();
}

void SignUp::setFullName(QString str)
{
    ui->inputFullName->setText(str);
}

void SignUp::setEmail(QString  str)
{
    ui->inputEmail->setText(str);
}

void SignUp::setPassword(QString  str)
{
    ui->inputPassword->setText(str);
}

void SignUp::setConfirmPassword(QString str)
{
    ui->inputConfirmPassword->setText(str);
}

QPushButton* SignUp::getButton()
{
    return ui->btnSignUp;
}

void SignUp::closeAndSendSignal()
{
    this->close();
    emit viewSignInShow();
}

