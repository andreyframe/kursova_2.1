#include "signservice.h"

SignService::SignService()
{

}

SellerModel* SignService::registerUser(SellerModel* seller) {
    int size = Data<SellerModel>::getCount();
    qDebug() << size;
    for(int i = 0; i < size; ++i){
        if(Data<SellerModel>::getByIndex(i)->getEmail() == seller->getEmail()){
            return NULL;
        }
    }
    return Data<SellerModel>::add(seller);
}

AdminModel* SignService::registerUser(AdminModel* admin) {
    int size = Data<AdminModel>::getCount();
    qDebug() << size;
    for(int i = 0; i < size; ++i){
        if(Data<AdminModel>::getByIndex(i)->getEmail() == admin->getEmail()){
            return NULL;
        }
    }
    return Data<AdminModel>::add(admin);
}

User SignService::loginUser(QString email, QString password)
{
    int size_seller = Data<SellerModel>::getCount();
    int size_admin = Data<AdminModel>::getCount();
    User temp("NULL", "NULL", "NULL", "NULL");
    for(int i = 0; i < size_seller; ++i){
        User user = *(Data<SellerModel>::getByIndex(i));
        if(user.getEmail() == email && user.getPassword() == password){
            return user;
        }
    }
    for(int i = 0; i < size_admin; ++i){
        User user = *(Data<AdminModel>::getByIndex(i));
        if(user.getEmail() == email && user.getPassword() == password){
            return user;
        }
    }
    return temp;
}
