#ifndef SIGNSERVICE_H
#define SIGNSERVICE_H

#include <QDebug>
#include <QString>
#include "service/data.h"
#include "models/user.h"

class SignService
{
public:
    SignService();
    SellerModel* registerUser(SellerModel*);
    AdminModel* registerUser(AdminModel*);
    User loginUser(QString, QString);
};

#endif // SIGNSERVICE_H
