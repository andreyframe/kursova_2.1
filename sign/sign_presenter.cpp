#include "sign_presenter.h"

Sign_Presenter::Sign_Presenter(SignIn* view_in, SignUp* view_up, SignService* s, IMessageService* msg) :
    reg_exp()
{
    view_signin = view_in;
    view_signup = view_up;
    view_signup->getButton()->setEnabled(false);

    service = s;
    message = msg;

    QObject::connect(view_in, SIGNAL(viewSignUpShow()), this, SIGNAL(viewSignUpShow()));
    QObject::connect(this, SIGNAL(viewSignUpShow()), view_up, SLOT(show()));
    QObject::connect(this, SIGNAL(viewSignUpClose()), view_up, SLOT(close()));

    QObject::connect(view_up, SIGNAL(viewSignInShow()), this, SIGNAL(viewSignInShow()));
    QObject::connect(this, SIGNAL(viewSignInShow()), view_in, SLOT(show()));
    QObject::connect(this, SIGNAL(viewSignInClose()), view_in, SLOT(close()));

    QObject::connect(view_up, SIGNAL(clickSignUp()), this, SIGNAL(clickSignUp()));
    QObject::connect(this, SIGNAL(clickSignUp()), this, SLOT(clickedSignUp()));

    QObject::connect(view_up, SIGNAL(changeForm()), this, SIGNAL(changeSignUpForm()));
    QObject::connect(this, SIGNAL(changeSignUpForm()), this, SLOT(changedForm()));

    QObject::connect(view_in, SIGNAL(clickedSignIn()), this, SLOT(showUserView()));
}

void Sign_Presenter::clickedSignUp()
{
    if(!view_signup->getButton()->isEnabled()) {
        message->ShowError("Undefined error!");
        return;
    }
    QString whoRegister = view_signup->getRole();
    if(whoRegister == "Administrator") {
        if(service->registerUser(new AdminModel(view_signup->getFullName(),
                                              view_signup->getEmail(),
                                              view_signup->getPassword(),
                                                 "Admin")) == NULL) {
            message->Information("Email exist!");
            return;
        }
    } else if(whoRegister == "Seller"){
        if(service->registerUser(new SellerModel(view_signup->getFullName(),
                                              view_signup->getEmail(),
                                              view_signup->getPassword(),
                                                 "Seller")) == NULL) {
            message->Information("Email exist!");
            return;
        }
    } else {
        message->Information("Please check your \"Role\".");
        return;
    }

    view_signup->setFullName("");
    view_signup->setEmail("");
    view_signup->setPassword("");
    view_signup->setConfirmPassword("");

    emit viewSignInShow();
    emit viewSignUpClose();
}

void Sign_Presenter::changedForm()
{
    if(reg_exp.name.indexIn(view_signup->getFullName()) != -1 &&
            reg_exp.email.indexIn(view_signup->getEmail()) != -1 &&
            reg_exp.password.indexIn(view_signup->getPassword()) != -1 &&
            view_signup->getPassword() == view_signup->getConfirmPassword()
            )
        view_signup->getButton()->setEnabled(true);
    else
        view_signup->getButton()->setEnabled(false);
}

void Sign_Presenter::showUserView()
{
    User user = this->service->loginUser(view_signin->getEmail(), view_signin->getPassword());
    QString whoLogin = user.getRole();
    if(whoLogin == "NULL") {
        message->Information("Please check your email and password.");
        return;
    }
    else if(whoLogin == "Admin"){
        Administrator* view_admin = new Administrator();
        AddFood* view_food = new AddFood();
        AdminService* service = new AdminService();
        MessageService* message = new MessageService();
        AdminPresenter* pres = new AdminPresenter(view_admin, view_food, service, message);
        view_admin->sendSignal();
        view_admin->show();
        emit viewSignInClose();
    } else if(whoLogin == "Seller"){
        Seller* view_seller = new Seller(user);
        SellerService* service = new SellerService();
        MessageService* message = new MessageService();
        SellerPresenter* pres = new SellerPresenter(view_seller, service, message);
        view_seller->show();
        emit viewSignInClose();
    } else {
        message->ShowError("Undefined error");
        return;
    }
    delete this;
}




















