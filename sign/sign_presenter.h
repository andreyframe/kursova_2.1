#ifndef SIGN_PRESENTER_H
#define SIGN_PRESENTER_H

#include <QObject>
#include <QString>
#include "signin.h"
#include "signup.h"
#include "signservice.h"
#include "service/messageservice.h"
#include "service/utilservice.h"
#include "service/data.h"

#include "seller/seller.h"
#include "seller/seller_presenter.h"
#include "seller/sellerservice.h"

#include "administrator/administrator.h"
#include "administrator/addfood.h"
#include "administrator/admin_presenter.h"
#include "administrator/admin_service.h"

class Sign_Presenter : public QObject
{
    Q_OBJECT

public:
    Sign_Presenter(SignIn*, SignUp*, SignService*, IMessageService*);

private slots:
    void clickedSignUp();
    void changedForm();
    void showUserView();

signals:
    void viewSignUpShow();
    void viewSignUpClose();
    void viewSignInShow();
    void viewSignInClose();
    void clickSignUp();
    void changeSignUpForm();

private:
    ISignIn* view_signin;
    ISignUp* view_signup;
    SignService* service;
    IMessageService* message;
    UtilService reg_exp;
};

#endif // SIGN_PRESENTER_H
