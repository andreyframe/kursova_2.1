#include "sellermodel.h"

SellerModel::SellerModel()
{

}

SellerModel::SellerModel(QString name, QString email, QString password, QString role) :
    User(name, email, password, role)//,
    //list_food()
{
    total = 0;
}
/*
void SellerModel::addFood(int id){
    list_food.push_back(id);
}
*/
float SellerModel::getTotal()
{
    return total;
}

void SellerModel::setTotal(float sum)
{
    total = sum;
}

QTextStream& operator >>(QTextStream &stream, SellerModel &obj)
{
    stream >> obj.id >> obj.full_name >> obj.email >> obj.password >> obj.role >> obj.total;
    return stream;
}

QTextStream& operator <<(QTextStream &stream, const SellerModel &obj)
{
    stream << obj.id << "\n" << obj.full_name << "\n" << obj.email << "\n" << obj.password << "\n" << obj.role << "\n" << obj.total << "\n";
    return stream;
}
/*
void SellerModel::test()
{
    std::for_each(list_food.begin(), list_food.end(), [](int i){
        qDebug() << i;
    });
    qDebug() << "===============";
}*/
