#ifndef ADMINMODEL_H
#define ADMINMODEL_H

#include <QString>
#include <list>
#include <QTableWidgetItem>
#include <QTextStream>
#include "user.h"

class AdminModel : public User
{
private:
public:
    AdminModel();
    AdminModel(QString, QString, QString, QString);
    friend QTextStream& operator >>(QTextStream&, AdminModel&);
    friend QTextStream& operator <<(QTextStream&, const AdminModel&);
};

#endif // ADMINMODEL_H
