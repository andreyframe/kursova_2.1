#include "foodmodel.h"

FoodModel::FoodModel()
{

}

FoodModel::FoodModel(QString name, float price)
{
    this->name = name;
    this->price = price;
}

QString FoodModel::getName()
{
    return name;
}

float FoodModel::getPrice()
{
    return price;
}

bool FoodModel::operator <(const FoodModel food) const
{
    return this->name < food.name;
}

QTextStream& operator >>(QTextStream &stream, FoodModel &obj)
{
    stream >> obj.id >> obj.name >> obj.price;
    return stream;
}

QTextStream& operator <<(QTextStream &stream, const FoodModel &obj)
{
    stream << obj.id << "\n" << obj.name << "\n" << obj.price << "\n";
    return stream;
}

