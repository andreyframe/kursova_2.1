#include "baseentity.h"

BaseEntity::BaseEntity()
{
    id = -1;
}

void BaseEntity::setId(int id)
{
    this->id = id;
}

int BaseEntity::getId()
{
    return id;
}

