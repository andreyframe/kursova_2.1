#ifndef USER_H
#define USER_H

#include <QString>
#include "baseentity.h"

class User : public BaseEntity
{
protected:
    QString full_name;
    QString email;
    QString password;
    QString role;
public:
    User();
    User(QString, QString, QString, QString);
    QString getName();
    QString getEmail();
    QString getPassword();
    QString getRole();
};

#endif // USER_H
