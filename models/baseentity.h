#ifndef BASEENTITY_H
#define BASEENTITY_H


class BaseEntity
{
protected:
    int id;
public:
    BaseEntity();
    void setId(int);
    int getId();
};

#endif // BASEENTITY_H
