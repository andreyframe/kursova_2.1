#include "user.h"

User::User() : BaseEntity()
{

}

User::User(QString full_name, QString email, QString password, QString role) :
    BaseEntity()
{
    this->full_name = full_name;
    this->email = email;
    this->password = password;
    this->role = role;
}

QString User::getName()
{
    return full_name;
}

QString User::getEmail()
{
    return email;
}

QString User::getPassword()
{
    return password;
}

QString User::getRole()
{
    return role;
}
