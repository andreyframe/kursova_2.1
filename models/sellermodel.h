#ifndef SELLERMODEL_H
#define SELLERMODEL_H

#include <QString>
#include <list>
#include "user.h"
#include <QTextStream>

#include <algorithm>
#include <QDebug>

class SellerModel : public User
{
private:
    //std::list<int> list_food;
    float total;
public:
    SellerModel();
    SellerModel(QString, QString, QString, QString);
    //void addFood(int);
    float getTotal();
    void setTotal(float);
    friend QTextStream &operator >>(QTextStream&, SellerModel&);
    friend QTextStream &operator <<(QTextStream&, const SellerModel&);
    //void test();
};

#endif // SELLERMODEL_H
