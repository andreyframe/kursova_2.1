#ifndef FOODMODEL_H
#define FOODMODEL_H

#include <QString>
#include <QTextStream>
#include "baseentity.h"

class FoodModel : public BaseEntity
{
private:
    QString name;
    float price;
public:
    FoodModel();
    FoodModel(QString, float);
    QString getName();
    float getPrice();
    bool operator <(const FoodModel) const;
    friend QTextStream &operator >>(QTextStream&, FoodModel&);
    friend QTextStream &operator <<(QTextStream&, const FoodModel&);
};

#endif // FOODMODEL_H
