#include "adminmodel.h"

AdminModel::AdminModel()
{

}

AdminModel::AdminModel(QString name, QString email, QString password, QString role) :
    User(name, email, password, role)
{

}

QTextStream& operator >>(QTextStream &stream, AdminModel &obj)
{
    stream >> obj.id >> obj.full_name >> obj.email >> obj.password >> obj.role;
    return stream;
}

QTextStream& operator <<(QTextStream &stream, const AdminModel &obj)
{
    stream << obj.id << "\n" << obj.full_name << "\n" << obj.email << "\n" << obj.password << "\n" << obj.role << "\n";
    return stream;
}
