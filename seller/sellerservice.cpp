#include "sellerservice.h"

SellerService::SellerService()
{

}

std::vector<FoodModel> SellerService::searchFood(QString text)
{
    std::vector<FoodModel> result;
    int size = Data<FoodModel>::getCount();
    for(int i = 0; i < size; ++i){
        if(Data<FoodModel>::getByIndex(i)->getName().toLower().contains(text.toLower()) && text != ""){
            result.push_back(*(Data<FoodModel>::getByIndex(i)));
        }
    }
    return result;
}

float SellerService::addOrder(int id, std::vector<FoodModel> order)
{
    SellerModel* seller = Data<SellerModel>::getById(id);
    std::vector<FoodModel>::iterator itr;
    float sum = 0;
    for(itr = order.begin(); itr != order.end(); ++itr){
        sum += itr->getPrice();
    }
    seller->setTotal(seller->getTotal() + sum);
    qDebug() << seller->getTotal();
    Data<SellerModel>::updateById(*seller, seller->getId());
    return sum;
}
