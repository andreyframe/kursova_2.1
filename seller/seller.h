#ifndef SELLER_H
#define SELLER_H

#include <QMainWindow>
#include <QStringList>
#include <QString>
#include <QWidgetItem>
#include <QModelIndexList>
#include <models/user.h>
#include <set>
#include "models/foodmodel.h"

#include <QDebug>

namespace Ui {
class Seller;
}

class ISellerView
{
public:
    void virtual addSearchList(QString) = 0;
    void virtual addTableList(FoodModel) = 0;
    int virtual selectedSearchItem() = 0;
    void virtual deleteItemTable(int) = 0;
    std::set<int> virtual selectedIndex() = 0;
    QString virtual getSearchText() = 0;
    void virtual clearList() = 0;
    void virtual clearTable() = 0;
    User virtual getUser() = 0;
};

class Seller : public QMainWindow, public ISellerView
{
    Q_OBJECT

public:
    explicit Seller(User, QWidget *parent = 0);
    ~Seller();
    void addSearchList(QString);
    void addTableList(FoodModel);
    int selectedSearchItem();
    void deleteItemTable(int);
    std::set<int> selectedIndex();
    QString getSearchText();
    void clearList();
    void clearTable();
    User getUser();

signals:
    void viewSignInShow();
    void changeLine();
    void pressBtnAdd();
    void pressBtnDelete();
    void doubleClickForDelete();
    void nextBuyer();

public slots:
    void closeAndSendSignal();

private:
    Ui::Seller *ui;
    User seller;
};

#endif // SELLER_H
