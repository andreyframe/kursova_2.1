#ifndef SELLERSERVICE_H
#define SELLERSERVICE_H

#include <QString>
#include <vector>
#include "service/data.h"
#include "models/foodmodel.h"

#include <QDebug>

class SellerService
{
public:
    SellerService();
    std::vector<FoodModel> searchFood(QString text);
    float addOrder(int, std::vector<FoodModel>);
};

#endif // SELLERSERVICE_H
