#ifndef SELLER_PRESENTER_H
#define SELLER_PRESENTER_H

#include <QObject>
#include <QString>
#include "seller.h"
#include "sellerservice.h"
#include "service/messageservice.h"
#include "sign/signin.h"
#include "sign/signup.h"
#include "sign/sign_presenter.h"
#include "sign/signservice.h"
#include "models/foodmodel.h"
#include <vector>
#include <set>
#include <iterator>

#include <QDebug>
#include <algorithm>

class SellerPresenter : public QObject
{
    Q_OBJECT
private:
    ISellerView* view;
    SellerService* service;
    IMessageService* message;
    std::vector<FoodModel> table_list;
    std::vector<FoodModel> search_list;
public:
    SellerPresenter(Seller*, SellerService*, MessageService*);

private slots:
    void closeAndOpenSignIn();
    void changeSearchLine();
    void addItemToTable();
    void deleteItemTable();
    void clearTable();

signals:
    void viewSellerClose();
};

#endif // SELLER_PRESENTER_H
