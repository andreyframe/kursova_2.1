#include "seller_presenter.h"

SellerPresenter::SellerPresenter(Seller *seller_view,
                                 SellerService *seller_service,
                                 MessageService *message_service) :
    table_list(),
    search_list()
{
    view = seller_view;
    service = seller_service;
    message = message_service;

    QObject::connect(seller_view, SIGNAL(viewSignInShow()), this, SIGNAL(viewSellerClose()));
    QObject::connect(this, SIGNAL(viewSellerClose()), this, SLOT(closeAndOpenSignIn()));

    QObject::connect(seller_view, SIGNAL(changeLine()), this, SLOT(changeSearchLine()));
    QObject::connect(seller_view, SIGNAL(pressBtnAdd()), this, SLOT(addItemToTable()));

    QObject::connect(seller_view, SIGNAL(pressBtnDelete()), this, SLOT(deleteItemTable()));
    QObject::connect(seller_view, SIGNAL(doubleClickForDelete()), this, SLOT(deleteItemTable()));

    QObject::connect(seller_view, SIGNAL(nextBuyer()), this, SLOT(clearTable()));
}

void SellerPresenter::closeAndOpenSignIn()
{
    SignIn* view_signin = new SignIn();
    SignUp* view_signup = new SignUp();
    SignService* service = new SignService();
    MessageService* message = new MessageService();
    Sign_Presenter* pres = new Sign_Presenter(view_signin, view_signup, service, message);
    view_signin->show();
    delete this;
}

void SellerPresenter::changeSearchLine()
{
    QString text = view->getSearchText();
    view->clearList();
    search_list.clear();
    search_list = service->searchFood(text);

    int size = search_list.size();
    for(int i = 0; i < size; ++i){
        view->addSearchList(search_list.at(i).getName());
    }
}

void SellerPresenter::addItemToTable()
{
    int index = view->selectedSearchItem();
    qDebug() << index;
    if(index == -1) {
        message->Information("Please select food!");
        return;
    }
    table_list.push_back(search_list.at(index));
    view->addTableList(search_list.at(index));
}

void SellerPresenter::deleteItemTable()
{
    std::set<int> indexs = view->selectedIndex();
    for(std::set<int>::reverse_iterator itr = indexs.rbegin(); itr != indexs.rend(); itr++){
        table_list.erase(table_list.begin() + *itr);
        view->deleteItemTable(*itr);
    }
    //std::for_each(table_list.begin(), table_list.end(), [](FoodModel x){
    //    qDebug() << x.getName();
    //});
}

void SellerPresenter::clearTable()
{

    float sum = service->addOrder(view->getUser().getId(), table_list);
    message->Information("Total sum: " + QString::number(sum));
    table_list.clear();
    view->clearTable();
}
