#include "seller.h"
#include "ui_seller.h"

Seller::Seller(User user, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Seller)
{
    ui->setupUi(this);
    this->seller = user;

    ui->tableWidget->setColumnCount(2);
    ui->tableWidget->setShowGrid(true);
    ui->tableWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Price" << "Name");

    ui->tableWidget->setColumnWidth(0, ui->tableWidget->width() * 0.7);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget->setEditTriggers(0);

    connect(ui->btn_exit, SIGNAL(clicked(bool)), this, SLOT(closeAndSendSignal()));
    connect(ui->btn_add, SIGNAL(clicked(bool)), this, SIGNAL(pressBtnAdd()));
    connect(ui->searchList, SIGNAL(doubleClicked(QModelIndex)), this, SIGNAL(pressBtnAdd()));
    connect(ui->tableWidget, SIGNAL(doubleClicked(QModelIndex)), this, SIGNAL(doubleClickForDelete()));
    connect(ui->btn_delete, SIGNAL(clicked(bool)), this, SIGNAL(pressBtnDelete()));
    connect(ui->input_search, SIGNAL(textEdited(QString)), this, SIGNAL(changeLine()));
    connect(ui->btn_next, SIGNAL(clicked(bool)), this, SIGNAL(nextBuyer()));
}

Seller::~Seller()
{
    delete ui;
}

void Seller::addSearchList(QString item)
{
    ui->searchList->addItem(item);
}

void Seller::addTableList(FoodModel item)
{
    int row = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(row);
    ui->tableWidget->setItem(row, 0, new QTableWidgetItem(QString::number((double)item.getPrice())));
    ui->tableWidget->setItem(row, 1, new QTableWidgetItem(item.getName()));
}

int Seller::selectedSearchItem()
{
    return ui->searchList->currentRow();
}

void Seller::deleteItemTable(int index)
{
    ui->tableWidget->removeRow(index);
}

std::set<int> Seller::selectedIndex()
{
    QModelIndexList selectedIndex = ui->tableWidget->selectionModel()->selectedRows();
    std::set<int> indexList;
    foreach(QModelIndex index, selectedIndex)
    {
        indexList.insert(index.row());
    }
    return indexList;
}

QString Seller::getSearchText()
{
    return ui->input_search->text();
}

void Seller::clearList()
{
    ui->searchList->clear();
}

void Seller::clearTable()
{
    int size = ui->tableWidget->rowCount();
    for(int i = 0; i < size; ++i){
        ui->tableWidget->removeRow(0);
    }
}

User Seller::getUser()
{
    return seller;
}

void Seller::closeAndSendSignal()
{
    this->close();
    emit viewSignInShow();
}
