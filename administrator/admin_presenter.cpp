#include "admin_presenter.h"

AdminPresenter::AdminPresenter(Administrator* view, AddFood* view_food, AdminService* service, MessageService* message)
{
    this->view = view;
    this->view_food = view_food;
    this->service = service;
    this->message = message;

    QObject::connect(view, SIGNAL(viewSignInShow()), this, SIGNAL(viewAdminClose()));
    QObject::connect(this, SIGNAL(viewAdminClose()), this, SLOT(closeAndOpenSignIn()));

    QObject::connect(view, SIGNAL(viewFoodShow()), this, SIGNAL(viewFoodShow()));
    QObject::connect(this, SIGNAL(viewFoodShow()), view_food, SLOT(show()));
    QObject::connect(this, SIGNAL(viewFoodShow()), view, SLOT(close()));

    QObject::connect(view_food, SIGNAL(viewAddFoodClose()), this, SIGNAL(viewFoodClose()));
    QObject::connect(this, SIGNAL(viewFoodClose()), view, SLOT(show()));

    QObject::connect(view_food, SIGNAL(pressAddBtn()), this, SLOT(addFoodToData()));

    QObject::connect(view, SIGNAL(initView()), this, SLOT(initTable()));
}

void AdminPresenter::closeAndOpenSignIn()
{
    SignIn* view_signin = new SignIn();
    SignUp* view_signup = new SignUp();
    SignService* service = new SignService();
    MessageService* message = new MessageService();
    Sign_Presenter* pres = new Sign_Presenter(view_signin, view_signup, service, message);
    view_signin->show();
    delete this;
}

void AdminPresenter::addFoodToData()
{
    QString name = view_food->getFoodName();
    float price = view_food->getFoodPrice();
    if(reg_exp.name.indexIn(name) == -1) {
        message->Information("Please enter correct name!");
        return;
    }
    service->addFood(new FoodModel(name, price));
    message->Information("The \"" + name + "\" was added at a price of " + QString::number(price));

    view_food->setFoodName("");
    view_food->setFoodPrice(0);
}

void AdminPresenter::initTable()
{
    qDebug() << "1";
    std::list<SellerModel> allSeller = service->getSellers();
    std::list<SellerModel>::iterator itr = allSeller.begin();
    std::list<SellerModel>::iterator end = allSeller.end();
    for(; itr != end; ++itr){
        qDebug() << itr->getEmail();
        view->addToTable(*itr);
    }
}










