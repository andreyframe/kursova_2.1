#include "administrator.h"
#include "ui_administrator.h"

Administrator::Administrator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Administrator)
{
    ui->setupUi(this);

    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setShowGrid(true);
    ui->tableWidget->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Name" << "Email" << "Total");

    ui->tableWidget->setColumnWidth(0, 200);
    ui->tableWidget->setColumnWidth(1, 200);
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget->setEditTriggers(0);

    connect(ui->btn_exit, SIGNAL(clicked(bool)), this, SLOT(closeAndSendSignal()));
    connect(ui->btn_add, SIGNAL(clicked(bool)), this, SIGNAL(viewFoodShow()));
}

Administrator::~Administrator()
{
    delete ui;
}

void Administrator::addToTable(SellerModel seller)
{
    int row = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(row);
    ui->tableWidget->setItem(row, 0, new QTableWidgetItem(seller.getName()));
    ui->tableWidget->setItem(row, 1, new QTableWidgetItem(seller.getEmail()));
    ui->tableWidget->setItem(row, 2, new QTableWidgetItem(QString::number(seller.getTotal())));

}

void Administrator::sendSignal()
{
    emit initView();
}

void Administrator::closeAndSendSignal(){
    this->close();
    emit viewSignInShow();
}
