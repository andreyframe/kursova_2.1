#ifndef ADDFOOD_H
#define ADDFOOD_H

#include <QWidget>
#include <QString>
#include <QDebug>

namespace Ui {
class AddFood;
}

class IFoodView {
public:
    QString virtual getFoodName() = 0;
    float virtual getFoodPrice() = 0;
    void virtual setFoodPrice(float) = 0;
    void virtual setFoodName(QString) = 0;
};

class AddFood : public QWidget, public IFoodView
{
    Q_OBJECT

public:
    explicit AddFood(QWidget *parent = 0);
    ~AddFood();
    QString getFoodName();
    float getFoodPrice();
    void setFoodPrice(float);
    void setFoodName(QString);

signals:
    void viewAddFoodClose();
    void pressAddBtn();

private slots:
    void closeAndShowAdmin();

private:
    Ui::AddFood *ui;
};

#endif // ADDFOOD_H
