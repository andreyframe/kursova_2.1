#include "addfood.h"
#include "ui_addfood.h"

AddFood::AddFood(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddFood)
{
    ui->setupUi(this);

    connect(ui->btn_back, SIGNAL(clicked(bool)), this, SLOT(closeAndShowAdmin()));
    connect(ui->btn_add, SIGNAL(clicked(bool)), this, SIGNAL(pressAddBtn()));
}

AddFood::~AddFood()
{
    delete ui;
}

QString AddFood::getFoodName()
{
    return ui->input_name->text();
}

float AddFood::getFoodPrice()
{
    return (float)ui->input_price->value();
}

void AddFood::setFoodPrice(float price)
{
    ui->input_price->setValue(price);
}

void AddFood::setFoodName(QString name)
{
    ui->input_name->setText(name);
}

void AddFood::closeAndShowAdmin()
{
    this->close();
    emit viewAddFoodClose();
}
