#ifndef ADMINISTRATOR_H
#define ADMINISTRATOR_H

#include <QMainWindow>
#include "models/sellermodel.h"

namespace Ui {
class Administrator;
}

class IAdminView {
public:
    void virtual addToTable(SellerModel) = 0;
};

class Administrator : public QMainWindow, public IAdminView
{
    Q_OBJECT

public:
    explicit Administrator(QWidget *parent = 0);
    ~Administrator();
    void addToTable(SellerModel);
    void sendSignal();

signals:
    void viewSignInShow();
    void viewFoodShow();
    void initView();

public slots:
    void closeAndSendSignal();

private:
    Ui::Administrator *ui;
};

#endif // ADMINISTRATOR_H
