#ifndef ADMIN_PRESENTER_H
#define ADMIN_PRESENTER_H

#include <QObject>
#include "administrator.h"
#include "admin_service.h"
#include "service/messageservice.h"
#include "sign/signin.h"
#include "sign/signup.h"
#include "sign/sign_presenter.h"
#include "sign/signservice.h"
#include "addfood.h"
#include "service/utilservice.h"

#include <QDebug>

class AdminPresenter : public QObject
{
    Q_OBJECT
private:
    IAdminView* view;
    IFoodView* view_food;
    AdminService* service;
    IMessageService* message;
    UtilService reg_exp;
public:
    AdminPresenter(Administrator*, AddFood*, AdminService*, MessageService*);

signals:
    void viewFoodClose();

private slots:
    void closeAndOpenSignIn();
    void addFoodToData();
    void initTable();
signals:
    void viewAdminClose();
    void viewFoodShow();
};

#endif // ADMIN_PRESENTER_H
