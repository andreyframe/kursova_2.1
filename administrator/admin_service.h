#ifndef ADMIN_SERVICE_H
#define ADMIN_SERVICE_H

#include "service/data.h"
#include "models/foodmodel.h"
#include "models/sellermodel.h"
#include <QString>

class AdminService
{
public:
    AdminService();
    FoodModel* addFood(FoodModel*);
    std::list<SellerModel> getSellers();
};

#endif // ADMIN_SERVICE_H
