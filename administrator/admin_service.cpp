#include "admin_service.h"

AdminService::AdminService()
{

}

FoodModel* AdminService::addFood(FoodModel* food)
{
    return Data<FoodModel>::add(food);
}

std::list<SellerModel> AdminService::getSellers()
{
    qDebug() << Data<SellerModel>::getCount();
    return Data<SellerModel>::getAll();
}
